                                         _ __
        ___                             | '  \
   ___  \ /  ___         ,'\_           | .-. \        /|
   \ /  | |,'__ \  ,'\_  |   \          | | | |      ,' |_   /|
 _ | |  | |\/  \ \ |   \ | |\_|    _    | |_| |   _ '-. .-',' |_   _
// | |  | |____| | | |\_|| |__    //    |     | ,'_`. | | '-. .-',' `. ,'\_
\\_| |_,' .-, _  | | |   | |\ \  //    .| |\_/ | / \ || |   | | / |\  \|   \
 `-. .-'| |/ / | | | |   | | \ \//     |  |    | | | || |   | | | |_\ || |\_|
   | |  | || \_| | | |   /_\  \ /      | |`    | | | || |   | | | .---'| |
   | |  | |\___,_\ /_\ _      //       | |     | \_/ || |   | | | |  /\| |
   /_\  | |           //_____//       .||`  _   `._,' | |   | | \ `-' /| |
        /_\           `------'        \ |  /-\ND _     `.\  | |  `._,' /_\
                                       \|        |HE         `.\
                                      __        _           _   __  _
                                     /   |__|  /_\  |\  /| |_) |_  |_)
                                     \__ |  | /   \ | \/ | |_) |__ | \
                                             _  _   _   __  _  _   __ ___ _
                                            (_)|-  (_` |_  /  |_) |_   | (_`
                                                   ._) |__ \_ | \ |__  | ._)



         __,---.__
      ,-'         `-.
    ,'               `.
   /                   \
  /         .           \
 ;           )           :
 |          ((           |
 |          ) \          |
 :         ( , )         ;
  \       _ `|'__       /
   \     ( """"_ )     /
    `.    )/(/( \|   ,'
      `- ()  )()|| -'
          | ()  ||
          |     ||
          |     ()
          |     |
          |     |
          |     |
          |     |
      ____|_____|____
     (________    ___)
        \___     _/
        (_____  __)
         \       /
          )__   (
         (____  _)
           |   |
           |   |
           |   |
           |   |
           |   |
           |   |
           |   |
         _/     \_
Stef .--'_________`--.

11401


    (
     )
    ()
   |--|
   |  |
 .-|  |-.
:  |  |  :
:  '--'  :
 '-....-'

   /\
  (  )
   ||
  |  |
  |  |
  |  |
  |  | ____
  |  |/ /  )  
 _|__|_/__/
(/cjr_\)

Art by Joan Stark

       (
       )\
       {_}
      .-;-.
     |'-=-'|
     |     |
     |     |
     |     |
     |     |
jgs  '.___.'

   )
  / (
 ( 6 )
 _`)'_
(  |  )
|`"-")|
|   ( )
|    (_)
|     |
|     |o!O

           *
           |
 * * * *   |   * * * *
  \ \ \ \__|__/ / / /
    \ \ \__|__/ / /
      \ \__|__/ /
        \__|__/
           |
           |
         __|__
        |     |  

Art by Joan Stark

            )
           (_)
          .-'-.
          |   |
          |   |
          |   |
          |   |
        __|   |__   .-.
     .-'  |   |  `-:   :
    :     `---'     :-'
jgs  `-._       _.-'
         '""""""

             .
           .....
       ..:::::::::.. 
    ..:::::: # ::::::..
  ..:::::: ====== :::::..
 ..::::: ========== ::::..
..::::: ==== |\ ==== ::::..
.::::: ==== /%#| ==== :::..
.::::: === |&@/ ==== :::::.
..::::: ===_\|_==== :::::..
 ..::::: /| .:. | :::::...
  ..:::: || .:. | ::::...
   ...:: (| .:. | ::....
     .... | .:. | ....

      |\
      / \
      \|/
     .=|-._
     |~'~~|)
     |    |
     |    |)
     |    |
 \// |    | \|/
  \//|..  |\//           Niki
_\\\\\\\__////////_
'/////////\\\\\\\\\`
        //\\
         //\

Art by Clinton James

        /\
       /  \
       \/\/
        ~|
       !~~-!
       |` ,!
       |'` |
       |   |
       |   |
       |   |
       |   |
       |   |
_______|___|_______
\                 /
 \_______________/

           :
   .       :       .
    ::.   :::   .::
       ::  :   ::
        :  )  :
   :::::  (_)  :::::
      .: .-;_ :.
    .:   |   \  :.
         |    )
         |   |     __
       . |   |   _) .(
      _\/|   | _) .'_|
       -\|   |) .'_(
     ~>>8o<<-'8o-'  `
      / /,\-(  )``.,
      | \.'\/  ) :(
      \   `'   `) (`
hjw    `._____.'`./

                 '
                  )                    `
                 /(l                   /)
                (  \                  / (
                ) * )                ( , )
                 \#/                  \#'
               .-"#'-.             .-"#"=,
            (  |"-.='|            '|"-,-"|
            )\ |     |  ,        /(|     | /(         ,
   (       /  )|     | (\       (  \     | ) )       ((
   )\     (   (|     | ) )      ) , )    |/ (        ) \
  /  )     ) . )     |/  (     ( # (     ( , )      /   )
 ( * (      \#/|     (`# )      `#/|     |`#/      (  '(
  \#/     .-"#'-.   .-"#'-,   .-"#'-.   .-=#"-;     `#/
.-"#'-.   |"=,-"|   |"-.-"|)  1"-.-"|   |"-.-"|   ,-"#"-.
|"-.-"|   |  !  |   |     |   |     |   |     !   |"-.-"|
|     |   |     |._,|     |   |     |._,|     a   |     |
|     |   |     |   |     |   |     |   |     p   |     |
|     |   |     |   |     |   |     |   |     x   |     |
'-._,-'   '-._,-'   '-._,-'   '-._,-'   '-._,-"   '-._,-'

Art by Joan Stark

   ,,;;;;;,,
 ,;;:::::::;;,
,;;::' , ':::;,
;;::  /(   ::;;
;;:: |  \  ::;;
';;::.\c/.::;;'
 ';:::'-,:::;'
   '';| |;''
      '-,
      | |
      '-,
      | |
      '-,
      | |
      '-,
      | |
jgs  /`"`\
  .-'.  _.'-.
 `._  `    _.'
    `"---"`
